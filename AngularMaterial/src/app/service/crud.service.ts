import { Injectable, OnInit } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable'

@Injectable()
export class CrudService implements OnInit {

  items: any;

  constructor(private db: AngularFireDatabase) {
    this.items = this.db.list<any>('items');
    console.log('itemsss', this.items);
  }

  ngOnInit() {
  }

  getItems() {
    return this.items;
  }

  getItemsObservable() {
    return this.db.list<any>('items').valueChanges();
  }

  saveItem(item: any) {
    this.items.push(item);
  }


}
