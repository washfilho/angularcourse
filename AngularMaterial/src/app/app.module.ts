import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatIconModule } from '@angular/material';

import { AppComponent } from './app.component';
import { CrudComponent } from './crud/crud.component';

import { CrudService } from './service/crud.service';

const appRoutes: Routes = [
  { path: 'crud', component: CrudComponent },
];

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDLQ6QBZraawHRAS38hSiz2KBOXGG_Hfrg",
    authDomain: "angular-course-a19dc.firebaseapp.com",
    databaseURL: "https://angular-course-a19dc.firebaseio.com",
    projectId: "angular-course-a19dc",
    storageBucket: "angular-course-a19dc.appspot.com",
    messagingSenderId: "179479100009"
  }
};

@NgModule({
  declarations: [
    AppComponent,
    CrudComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    AngularFireModule.initializeApp(environment.firebase, 'my-app'),
    AngularFireDatabaseModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule
  ],
  providers: [CrudService],
  bootstrap: [AppComponent]
})
export class AppModule { }
