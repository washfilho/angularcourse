import { Component, OnInit } from '@angular/core';

import { CrudService } from '../service/crud.service';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {

  name: string = '';
  on: boolean = false;
  items: any = [];

  constructor(private crudService: CrudService) {
    console.log('crud component');
   }

  ngOnInit() {
    console.log('on init crud component');
    this.items = this.crudService.getItemsObservable();
  }

  save(){
    let item = {
      name: this.name,
      on: this.on
    }
    this.crudService.saveItem(item);
  }

}
