import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ComponentTestComponent } from './component-test/component-test.component';

const appRoutes: Routes = [
  { path: 'test', component: ComponentTestComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ComponentTestComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
